const path = require('path')
const { ipcMain } = require('electron');
const electron = require('electron')
const app = electron.app
const fs = require('fs');
let settings = JSON.parse(fs.readFileSync(path.join(__dirname, 'config/default.json')));
console.log(settings)
const globalShortcut = electron.globalShortcut
const prompt = require('electron-prompt');
const BrowserWindow = electron.BrowserWindow;
var mainWindow;
app.on('ready', function () {
    prompt({
        title: 'INTRODUZCA SU URL',
        label: 'URL:',
        value: settings["page_url"],
        inputAttrs: {
            type: 'url'
        },
        type: 'input'
    })
    .then((r) => {
        if(r === null) {
            console.log('user cancelled');
        } else {
            console.log('result', r);
            mainWindow = new BrowserWindow({
                minWidth: 1200,
                minHeight: 700,
                resizable: true,
                show: true,
                scrollBounce: true,
                webPreferences: {
                    preload: path.join(__dirname, 'preload.js')
                }
            });
        
            mainWindow.webContents.loadURL(r) // load your web page
            mainWindow.webContents.openDevTools()
            globalShortcut.register('f5', function() {
                console.log('f5 is pressed')
                mainWindow.reload()
            })
            globalShortcut.register('CommandOrControl+R', function() {
                console.log('CommandOrControl+R is pressed')
                mainWindow.reload()
            })
        
            ipcMain.on('serverAlert', (event, msg) => {
                console.log(`Mensaje: ${msg}`)
                mainWindow.webContents.send('sendMessage', 'Mensaje recibido desde la aplicacion') // send to web page
            })
        
            ipcMain.on('serverPrintOut', (event, ticket) => {
                console.log(`Ticket Recibido: `, ticket) // msg from web page
        
                let win = new BrowserWindow({
                    show: false,
                    webPreferences: {
                        nodeIntegration: true
                    }
                });
        
                fs.writeFile(path.join(__dirname, 'config/print.txt'), ticket.content,
                    {
                        encoding: "utf8",
                        flag: "w",
                        mode: 0o666
                    },
                    (err) => {
                        if (err)
                            console.log(err);
                        else {
                            console.log("File written successfully\n");
                            console.log("The written has the following contents:");
                            win.loadFile(path.join(__dirname, 'config/print.txt'));
        
                            win.webContents.on('did-finish-load', () => {
                                win.webContents.print(ticket.options, (success, failureReason) => {
                                    if (!success) console.log(failureReason);
                                    console.log('Print Initiated');
                                });
                            });
                    
                        }
                    });
            })
        }
    })
    .catch(console.error);
})