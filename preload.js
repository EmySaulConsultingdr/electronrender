const path = require('path')
const { ipcRenderer } = require('electron');
const fs = require('fs');
let settings = JSON.parse(fs.readFileSync(path.join(__dirname, 'config/default.json')));
const { contextBridge } = require('electron');
const { default: axios } = require('axios');

contextBridge.exposeInMainWorld('DroidMAR', {
    getSettings: () => {
        return settings
    },
    getServerValid: async () => {
        try {
            var response = await axios.get(settings["page_url"])
            return response.status == 200
        } catch (ex) {
            return false
        }
    },
    serverAlert: (message) => {
        ipcRenderer.send('serverAlert', message)
        ipcRenderer.on('sendMessage', (event, msg) => {
            alert(msg)
        })
    },
    printOut: (content, options) => {
        ipcRenderer.send('serverPrintOut', {content, options})
        ipcRenderer.on('receivedTicket', (event, msg) => {
            alert(msg)
        })
    }

})